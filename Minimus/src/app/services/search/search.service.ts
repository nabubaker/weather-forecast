import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class SearchService {

  allCities: string[]
  matchingCities: string[]
  constructor(public http: HttpClient) {

    this.allCities = ['Paris', 'London', 'Nablus', 'Jenin', 'Jericho', 'Hebron', 'Ramallah', 'Abu Dis',
    `Bani Na'im`, 'Beitunia', 'al-Bireh', 'Deir al-Balah','Dura', 'Jabalia', 'Halhul', 'Khan Yunis', 'Qabatiya',
    'Qalqilya', 'Rafah', 'Surif', 'Tubas', 'Tulkarm', `Ya'bad`, 'al-Yamun']

    this.matchingCities = []
  }

  getMatchingCities(city: string): Array<string> {

    // In case of API service provided for the search
    // this.http.get(
    //   `https://API_URL?q=${city}&APPID=API_KEY`)
    //   .subscribe((data) => {
    //     dataSubject.next(data['weather'][0].main);
    //   });
    const cityRegex = new RegExp(`^${city}`, 'i');
    this.matchingCities = this.allCities.filter((matchingCity) => matchingCity.match(cityRegex))
    return this.matchingCities;
  }


}